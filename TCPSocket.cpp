#include "TCPSocket.h"

#include "SocketUtil.h"

TCPSocket::TCPSocket(const TCPSocket& other) :
	mSocket{other.mSocket}
{}


TCPSocket::TCPSocket(TCPSocket&& other) noexcept :
	mSocket{other.mSocket}
{
	other.mSocket = INVALID_SOCKET;
}


TCPSocket& TCPSocket::operator=(const TCPSocket& other)
{
	if (this == &other)
	{
		return *this;
	}

	mSocket = other.mSocket;
	return *this;
}


TCPSocket& TCPSocket::operator=(TCPSocket&& other) noexcept
{
	if (this == &other)
	{
		return *this;
	}

	mSocket = other.mSocket;
	other.mSocket = INVALID_SOCKET;
	return *this;
}


TCPSocket::~TCPSocket()
{
	closesocket(mSocket);
}


TCPSocket::operator bool() const
{
	return mSocket != INVALID_SOCKET;
}


int TCPSocket::Connect(const SocketAddress& inAddress) const
{
	int err = connect(mSocket, &inAddress.mSockAddr, inAddress.GetSize());
	
	if (err >= 0)
	{
		return NO_ERROR;
	}

	SocketUtil::ReportError(L"TCPSocket::Connect");
	return SocketUtil::GetLastError();
}


int TCPSocket::Bind(const SocketAddress& inBindAddress) const
{
	int err = bind(mSocket, &inBindAddress.mSockAddr, inBindAddress.GetSize());

	if (err == 0)
	{
		return NO_ERROR;
	}
	
	SocketUtil::ReportError(L"TCPSocket::Bind");
	return SocketUtil::GetLastError();
}


int TCPSocket::Listen(int inBackLog) const
{
	int err = listen(mSocket, inBackLog);
	
	if (err >= 0)
	{
		return NO_ERROR;
	}

	SocketUtil::ReportError(L"TCPSocket::Listen");
	return SocketUtil::GetLastError();
}


TCPSocketPtr TCPSocket::Accept(SocketAddress& inFromAddress) const
{
	int length = inFromAddress.GetSize();
	SOCKET newSocket = accept(mSocket, &inFromAddress.mSockAddr, &length);

	if (newSocket == INVALID_SOCKET)
	{
		SocketUtil::ReportError(L"TCPSocket::Accept");
		return nullptr;
	}

	return TCPSocketPtr{new TCPSocket{newSocket}};
}


int TCPSocket::Send(const void* inData, int inLen) const
{
	const int byteSentCount = send(mSocket, static_cast<const char*>(inData), inLen, 0);

	if (byteSentCount >= 0)
	{
		return byteSentCount;
	}
	SocketUtil::ReportError(L"TCPSocket::Send");
	return SocketUtil::GetLastError();
}


int TCPSocket::Receive(void* inData, int inLen) const
{
	int byteReceivedCount = recv(mSocket, static_cast<char*>(inData), inLen, 0);
	SocketUtil::ReportError(L"TCPSocket::Receive");
	return byteReceivedCount;
}
