#include "../Socket_Test.h"

#include <iostream>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h> // support for basic file logging
#include <spdlog/sinks/rotating_file_sink.h> // support for rotating file logging

using namespace std;

int main()
{
	if (int err = SocketUtil::Initialize();
		0 != err)
	{
		spdlog::error("초기화 실패");
		return err;
	}

	auto address = SocketAddressFactory::CreateIPv4FromString("127.0.0.1:48000");
	TCPSocketPtr clientSocket = SocketUtil::CreateTCPSocket(SocketAddressFamily::INET);

	if (int err = clientSocket->Connect(*address.get());
		0 != err)
	{
		spdlog::error("연결실패");
		return err;
	}

	string input(300, '\0'); // fill ctor
	while (true)
	{
		cin >> input;
		spdlog::error("로그 전송");
		clientSocket->Send(static_cast<const void*>(input.c_str()), 300);
	}
}

