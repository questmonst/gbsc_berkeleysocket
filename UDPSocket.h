#pragma once
#include <WinSock2.h>
#include <memory>

class SocketAddress;

class UDPSocket
{
public:
	UDPSocket(const UDPSocket& other);
	UDPSocket(UDPSocket&& other) noexcept;
	UDPSocket& operator=(const UDPSocket& other);
	UDPSocket& operator=(UDPSocket&& other) noexcept;

	~UDPSocket();

	int SetNonBlockingMode(bool inShouldBeNonBlocking) const;
	int Bind(const SocketAddress& inToAddress) const;
	int SendTo(const void* inData, int inLen, const SocketAddress& inTo) const;
	int ReceiveFrom(void* inBuffer, int inLen, SocketAddress& outFrom) const;

private:
	friend class SocketUtil;
	UDPSocket(SOCKET inSocket) : mSocket{inSocket} {}
	
	SOCKET mSocket;
};

using UDPSocketPtr = std::shared_ptr<UDPSocket>;

