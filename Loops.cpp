#include "UDPSocket.h"
#include "LoopFunctions.h"
#include "TCPSocket.h"
#include "SocketUtil.h"

static bool gIsGameRunning = true;

void LoopFunctions::DoGameLoop()
{
	UDPSocketPtr mySock = SocketUtil::CreateUDPSocket(INET);
	mySock->SetNonBlockingMode(true);

	while (gIsGameRunning)
	{
		char data[1500];
		SocketAddress socketAddress;

		int bytesReceived = mySock->ReceiveFrom(data, sizeof(data), socketAddress);
		if (bytesReceived > 0)
		{
			ProcessReceivedData(data, bytesReceived, socketAddress);

		}
		DoGameLoop();
	}
}

void LoopFunctions::DoTCPLoop()
{
	TCPSocketPtr listenSocket = SocketUtil::CreateTCPSocket(INET);
	SocketAddress receivingAddress(INADDR_ANY, 48000);

	if (listenSocket->Bind(receivingAddress) != NO_ERROR)
	{
		return;
	}

	std::vector<TCPSocketPtr> readBlockSockets;
	readBlockSockets.push_back(listenSocket);
	std::vector<TCPSocketPtr> readableSockets;

	while (gIsGameRunning)
	{
		if (!SocketUtil::Select(&readBlockSockets, &readableSockets,
			nullptr, nullptr, nullptr, nullptr))
			continue;


		for (const TCPSocketPtr& socket : readableSockets)
		{
			if (socket == listenSocket)
			{
				SocketAddress newClientAddress{1};
				auto newSocket = listenSocket->Accept(newClientAddress);
				readBlockSockets.push_back(newSocket);
				ProcessNewClient(newSocket, newClientAddress);
			}
			else
			{
				char segment[GOOD_SEGMENT_SIZE];
				int dataReceive = socket->Receive(segment, GOOD_SEGMENT_SIZE);
				if (dataReceive > 0)
				{
					ProcessDataFromClient(socket, segment, dataReceived);
				}
			}
		}

	}
}