#pragma once
#define WIN32_LEAN_AND_MEAN 

#include <memory>

#include <WS2tcpip.h>
#include <WinSock2.h>

#pragma comment(lib, "ws2_32.lib") 

class SocketAddress
{
public:
	SocketAddress(uint32_t inAddress, uint16_t inPort);

	SocketAddress(const sockaddr& inSockAddr);

	SocketAddress() = default;//�ӽ� ����

	size_t GetSize() const;

private:
	friend class UDPSocket;
	friend class TCPSocket;
	sockaddr_in* GetAsSockAddrIn();

	sockaddr mSockAddr;
};

using SocketAddressPtr = std::shared_ptr<SocketAddress>;
