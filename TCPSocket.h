#pragma once
#include <memory>

#define WIN32_LEAN_AND_MEAN
#include <WinSock2.h>

#include "SocketAddress.h"

class SocketAddress;
//장점 : shared일일이 칠 필요가 X//단점:전방선언이 훨씬 빠름pch에 몰아넣는게 낫다.

class TCPSocket;
using TCPSocketPtr = std::shared_ptr<TCPSocket>;

class TCPSocket
{
public:
	TCPSocket(const TCPSocket& other);
	TCPSocket(TCPSocket&& other) noexcept;

	TCPSocket& operator=(const TCPSocket& other);
	TCPSocket& operator=(TCPSocket&& other) noexcept;

	~TCPSocket();

	explicit operator bool() const;
	
	int Connect(const SocketAddress& inAddress) const;
	int Bind(const SocketAddress& inToAddress) const;
	int Listen(int inBackLog = 32) const;
	TCPSocketPtr Accept(SocketAddress& inFromAddress) const;
	int Send(const void* inData, int inLen) const;
	int Receive(void* inBuffer, int inLen) const;
	
private:
	friend class SocketUtil;
	TCPSocket(SOCKET inSocket) : mSocket{inSocket} {}
	
	SOCKET mSocket;
};

//복사생성자 이동대입연산자 이동생성자 복사 대입연산자
