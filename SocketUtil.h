#pragma once

#include <vector>

#include "UDPSocket.h"
#include "TCPSocket.h"

enum class SocketAddressFamily : int //타입지정
{
	INET = AF_INET,
	INET6 = AF_INET6
}; //enum 보다 enum class를 쓰자. ->타입 안정성 있는 enum//

class SocketUtil
{
public:
	SocketUtil() = delete;

	static int Initialize(); // winsock 초기화
	static void Terminate(); // winsock 종료

	static void ReportError(const wchar_t* errorMessage);
	static int GetLastError();
	static UDPSocketPtr CreateUDPSocket(SocketAddressFamily inFamily);
	static TCPSocketPtr CreateTCPSocket(SocketAddressFamily inFamily);
	static void FillSetFromVector(fd_set& outSet, const std::vector<TCPSocketPtr>& inSockets);
	static void FillVectorFromSet(
		std::vector<TCPSocketPtr>& outSockets, const std::vector<TCPSocketPtr>& inSockets,
		const fd_set& inSet
	);

	static int Select(
		const std::vector<TCPSocketPtr>& inReadSet, std::vector<TCPSocketPtr>& outReadSet,
		const std::vector<TCPSocketPtr>& inWriteSet, std::vector<TCPSocketPtr>& outWriteSet,
		const std::vector<TCPSocketPtr>& inExceptSet, std::vector<TCPSocketPtr>& outExceptSet
	);
	static int Select(const std::vector<TCPSocketPtr>& inReadSet, std::vector<TCPSocketPtr>& outReadSet);
};
