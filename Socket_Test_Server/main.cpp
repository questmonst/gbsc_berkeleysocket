#include "../Socket_Test.h"

#include <iostream>

#include <spdlog/spdlog.h>
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
#include "spdlog/sinks/rotating_file_sink.h" // support for rotating file logging

using namespace std;

int main()
{
	if (int err = SocketUtil::Initialize();
		0 != err)
	{
		spdlog::error("이니셜라이즈 실패");
		return err;
	}

	TCPSocketPtr listenSocket = SocketUtil::CreateTCPSocket(SocketAddressFamily::INET);
	SocketAddress receivingAddress{ INADDR_ANY, 48000 };
	//리슨 소켓 만들고, 받는 주소 설정

	if (int err = listenSocket->Bind(receivingAddress);
		SOCKET_ERROR == err)
	{
		spdlog::error("바인딩 실패");
		return err;
	}
	else
	{
		spdlog::info("바인딩 성공");
	}

	if (int err = listenSocket->Listen();
		SOCKET_ERROR == err)
	{
		spdlog::error("리스닝 실패");
		return err;
	}
	else
	{
		spdlog::info("리스닝 성공");
	}

	char buffer[301];
	SocketAddress clientAddress{};

	while (true)
	{
		spdlog::info("어셉트 받는중...");

		auto clientSocket = listenSocket->Accept(clientAddress);
		if (!clientSocket)
		{
			spdlog::error("어셉트 실패: {}", SocketUtil::GetLastError());
			break;
		}
		else
		{
			spdlog::info("어셉트 성공");
		}

		while (true)
		{
			spdlog::info("리시브 받는중");

			int received = 0;
			if (received = clientSocket->Receive(static_cast<void*>(buffer), 300);
				SOCKET_ERROR == received)
			{
				spdlog::error("리시브 실패: {}", received);
				break;
			}
			// 받은 게 없는 경우
			else if (received == 0)
			{
				spdlog::info("리시브 받은 것 없음 : {}", received);

				break;
			}
			else
			{
				spdlog::info("리시브: {}", received);
			}


			spdlog::info("{}", buffer);
		}
	}

	SocketUtil::Terminate();
	return EXIT_SUCCESS;
}
