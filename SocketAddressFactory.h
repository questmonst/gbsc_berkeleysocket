#pragma once
#include <string>
#include "SocketAddress.h"

class SocketAddressFactory
{
public:
	static SocketAddressPtr CreateIPv4FromString(const std::string& /*std::string_view*/ inString);
	SocketAddressFactory() = delete;//인스턴스 생성이 불가능
};