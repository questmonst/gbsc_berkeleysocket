#include "SocketUtil.h"

#include <iostream>

int SocketUtil::Initialize()
{
	WSADATA wsaData{};
	int toRet = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (toRet != 0)
	{
		ReportError(L"SocketUtil::Initialize");
	}
	return toRet;
}

void SocketUtil::Terminate()
{
	WSACleanup();
}

void SocketUtil::ReportError(const wchar_t* errorMessage)
{
	std::wcout << errorMessage << std::endl;
}

int SocketUtil::GetLastError()
{
	return WSAGetLastError(); //window��
}

void SocketUtil::FillSetFromVector(fd_set& outSet, const std::vector<TCPSocketPtr>& inSockets)
{
	FD_ZERO(&outSet);
	for (const TCPSocketPtr& socket : inSockets)
	{
		FD_SET(socket->mSocket, &outSet);
	}
}

TCPSocketPtr SocketUtil::CreateTCPSocket(SocketAddressFamily inFamily)
{
	SOCKET s = socket(static_cast<int>(inFamily), SOCK_STREAM, 0);
	if (s != INVALID_SOCKET)
	{
		return TCPSocketPtr{new TCPSocket{s}};
	}

	ReportError(L"SocketUtil::CreateTCPSocket");
	return nullptr;
}

UDPSocketPtr SocketUtil::CreateUDPSocket(SocketAddressFamily inFamily)
{
	SOCKET s = socket(static_cast<int>(inFamily), SOCK_DGRAM, IPPROTO_UDP);
	if (s != INVALID_SOCKET)
	{
		return UDPSocketPtr{new UDPSocket{s}};
	}

	ReportError(L"SocketUtil::CreateUDPSocket");
	return nullptr;
}

void SocketUtil::FillVectorFromSet(
	std::vector<TCPSocketPtr>& outSockets, const std::vector<TCPSocketPtr>& inSockets,
	const fd_set& inSet
)
{
	outSockets.clear();
	for (auto&& socket : inSockets)
	{
		if (FD_ISSET(socket->mSocket, &inSet))
		{
			outSockets.push_back(socket);
		}
	}
}


int SocketUtil::Select(
	const std::vector<TCPSocketPtr>& inReadSet, std::vector<TCPSocketPtr>& outReadSet,
	const std::vector<TCPSocketPtr>& inWriteSet, std::vector<TCPSocketPtr>& outWriteSet,
	const std::vector<TCPSocketPtr>& inExceptSet, std::vector<TCPSocketPtr>& outExceptSet
)
{
	fd_set read, write, except;
	FillSetFromVector(read, inReadSet);
	FillSetFromVector(read, inWriteSet);
	FillSetFromVector(read, inExceptSet);

	int toRet = select(0, &read, &write, &except, nullptr);

	if (toRet > 0)
	{
		FillVectorFromSet(outReadSet, inReadSet, read);
		FillVectorFromSet(outWriteSet, inWriteSet, write);
		FillVectorFromSet(outExceptSet, inExceptSet, except);
	}
	return toRet;
}

int SocketUtil::Select(
	const std::vector<TCPSocketPtr>& inReadSet, std::vector<TCPSocketPtr>& outReadSet)
{
	fd_set read, write, except;
	FillSetFromVector(read, inReadSet);

	int toRet = select(0, &read, &write, &except, nullptr);

	if (toRet > 0)
	{
		FillVectorFromSet(outReadSet, inReadSet, read);
	}
	return toRet;
}
