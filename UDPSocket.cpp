#include "UDPSocket.h"

#include "SocketAddress.h"
#include "SocketUtil.h"

int UDPSocket::Bind(const SocketAddress& inBindAddress) const
{
	int err = bind(mSocket, &inBindAddress.mSockAddr, inBindAddress.GetSize());

	if (err == 0)
	{
		return NO_ERROR;
	}

	SocketUtil::ReportError(L"UDPSocket::Bind");
	return SocketUtil::GetLastError();
}

int UDPSocket::SendTo(const void* inData, int inLen, const SocketAddress& inTo) const
{
	const int byteSendCount = sendto(mSocket, static_cast<const char*>(inData), inLen, 0, &inTo.mSockAddr,
									inTo.GetSize());

	if (byteSendCount >= 0)
	{
		return byteSendCount;
	}

	SocketUtil::ReportError(L"UDPSocket::SendTo");
	return SocketUtil::GetLastError(); //에러코드를 음수로 리턴함
}

int UDPSocket::ReceiveFrom(void* inBuffer, int inMaxLength, SocketAddress& outFromAddress) const
{
	int fromLength = outFromAddress.GetSize();
	int readByteCount = recvfrom(mSocket, static_cast<char*>(inBuffer), inMaxLength, 0, &outFromAddress.mSockAddr,
								&fromLength);

	if (readByteCount >= 0)
	{
		return readByteCount;
	}

	SocketUtil::ReportError(L"UDPSocket::ReceiveFrom");
	return SocketUtil::GetLastError();
}

UDPSocket::UDPSocket(const UDPSocket& other) :mSocket{other.mSocket}
{}


UDPSocket::UDPSocket(UDPSocket&& other) noexcept :
	mSocket{other.mSocket}
{
	other.mSocket = INVALID_SOCKET;
}

UDPSocket& UDPSocket::operator=(const UDPSocket& other)
{
	if (this == &other)
	{
		return *this;
	}
	mSocket = other.mSocket;
	return *this;
}

UDPSocket& UDPSocket::operator=(UDPSocket&& other) noexcept
{
	if (this == &other)
	{
		return *this;
	}
	mSocket = other.mSocket;
	other.mSocket = INVALID_SOCKET;
	return *this;
}

UDPSocket::~UDPSocket()
{
	closesocket(mSocket);
}

int UDPSocket::SetNonBlockingMode(bool inShouldBeNonBlocking) const
{
	u_long arg = inShouldBeNonBlocking;
	int result = ioctlsocket(mSocket, FIONBIO, &arg);

	if (result != SOCKET_ERROR)
	{
		return NO_ERROR;
	}

	SocketUtil::ReportError(L"UDPSocket::SetNonBlockingMode");
	return SocketUtil::GetLastError();
} //3-8
